# _*_ coding: UTF-8 _*_
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time

# ====== 開牌結果  ======
# 龍虎 R-B
# [ "1K", "3J" ] 龍、龍單、龍紅、龍大、虎單、虎紅、虎大
# [ "42", "24" ] 虎、虎雙、虎黑、虎小、龍雙、龍黑、龍小
# [ "26", "46" ] 和
# [ "1K", "3J" ] 龍、龍單、龍紅、龍大、虎單、虎紅、虎大
# [ "42", "24" ] 虎、虎雙、虎黑、虎小、龍雙、龍黑、龍小
# [ "26", "46" ] 和

game_round1 = [
    "tiger-odd",  # 虎單
    "tiger-red",  # 虎紅
    "tiger-big",  # 虎大
    "dragon",  # 龍
    "dragon-big",  # 龍大
    "dragon-red",  # 龍紅
    "dragon-odd",  # 龍單
]

game_round2 = [
    "tiger-even",  # 虎雙
    "tiger-black",  # 虎黑
    "tiger-small",  # 虎小
    "tiger",  # 虎
    "dragon-even",  # 龍雙
    "dragon-black",  # 龍黑
    "dragon-small",  # 龍小
]

game_round3 = "tie",  # 和

all_game_round = {
    "game_round1": False,
    "game_round2": False,
    "game_round3": False,
}


class LiveDragonTigerAutoBet:
    """ RG真人_龍虎自動下注 """

    def __init__(self, driver, get_img):
        self.driver = driver
        self.get_img = get_img

    def check_all_round(self, all_game):
        """
        確認所有下注注區是否為 True
        參數:
            game (dict): 遊戲注區

        Returns:
            bool: 判斷全部成功回傳 True，失敗則是 False
        """

        result_list = [all_game[i] for i in all_game]
        return False if "False" in str(result_list) else True

    def check_bet_success(self):
        """ 確認下注是否成功 """

        try:
            # 點擊確認按鈕
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//img[@src='img/confirm_button.5fe259e2.svg']"))).click()

            # 下注成功
            WebDriverWait(self.driver, 30).until(EC.presence_of_element_located(
                (By.XPATH, "//div[contains(text(), '下注成功')]")))
            self.get_img("下注成功")

        except:
            print("下注失敗\n")

    def auto_bet(self):
        """ 自動下注 """

        bet_finish = False
        result_list = []

        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//div[@class='nav']//a[./span[text()='真人']]"))).click()

        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//div[@class='reg_live rg']//a[@class='lnk_enterGame']"))).click()
        time.sleep(1)

        # wait loading
        WebDriverWait(self.driver, 10).until(EC.invisibility_of_element_located(
            (By.XPATH, "//div[@class='box_bg box_loading' and not(contains(@style, 'none'))]")))
        time.sleep(1)

        handles = self.driver.window_handles
        self.driver.switch_to.window(handles[-1])

        # 點擊龍虎頁簽
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//div[@class='club']//div[text()='龍虎']"))).click()

        try:
            # 進R-B桌
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[@class='desk-info']/div[./span[text()=' 龍虎 R-B']]"))).click()
            time.sleep(2)

        except Exception as e:
            self.get_img("Fail：龍虎 R-B 進桌失敗")
            print(e)
            return False

        try:
            while not bet_finish:
                # 判斷是否皆下注完成
                bet_finish = self.check_all_round(all_game_round)

                # 局號
                _ele = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                    (By.XPATH, "//div[@class='game-name']")))
                # 計算餘數，連開6局，所以總共為6
                bet_round = (int(_ele.text[-4:])) % 6

                # 倒數計時
                bet_sec = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                    (By.XPATH, "//div[@class='timer-wrapper']"))).text

                # 部等於 0 且大於 3 秒才進行下注
                if bet_sec and int(bet_sec) > 3:
                    if bet_round == 1:
                        print("下注注區：龍、龍單、龍紅、龍大、虎單、虎紅、虎大")

                        # 下注
                        for i in game_round1:
                            xpath = f"//*[name()='path' and @class='{i}'"
                            # 點擊
                            WebDriverWait(self.driver, 20).until(EC.presence_of_element_located(
                                (By.XPATH, f"//*[name()='svg' and @class='touch-panel']{xpath}]"))).click()

                        self.check_bet_success()
                        all_game_round['game_round1'] = True

                    elif bet_round == 2:
                        print("下注注區：虎、虎雙、虎黑、虎小、龍雙、龍黑、龍小")

                        # 下注
                        for i in game_round2:
                            xpath = f"//*[name()='path' and @class='{i}'"
                            # 點擊
                            WebDriverWait(self.driver, 20).until(EC.presence_of_element_located(
                                (By.XPATH, f"//*[name()='svg' and @class='touch-panel']{xpath}]"))).click()

                        self.check_bet_success()
                        all_game_round['game_round2'] = True

                    elif bet_round == 3:
                        print("下注注區：和")

                        # 注區
                        xpath = "//*[name()='path' and @class='tie']"

                        # 點擊
                        WebDriverWait(self.driver, 20).until(EC.presence_of_element_located(
                            (By.XPATH, f"//*[name()='svg' and @class='touch-panel']{xpath}"))).click()

                        self.check_bet_success()
                        all_game_round['game_round3'] = True

                    # 等開新局
                    WebDriverWait(self.driver, 30).until(EC.presence_of_element_located(
                        (By.XPATH, "//div[contains(text(), '已開局，請下注')]")))
                    time.sleep(1)

                else:
                    # 等開新局
                    WebDriverWait(self.driver, 30).until(EC.presence_of_element_located(
                        (By.XPATH, "//div[contains(text(), '已開局，請下注')]")))

        except Exception as e:
            self.get_img("Fail:「龍虎」自動下注失敗")
            print(e)
            result_list.append("False")

        results = False if "False" in str(result_list) else True
        return results
