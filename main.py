# _*_ coding: UTF-8 _*_
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
import time
import unittest
from Setting import *
from HTMLTestRunner import HTMLTestRunner
from datetime import datetime
import os
import Baccarat
import DragonTiger


options = webdriver.ChromeOptions()
options.add_experimental_option('excludeSwitches', ['enable-automation'])
driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()), options=options)
get_img = HTMLTestRunner().get_img
driver.maximize_window()
driver.get("https://ts.bacctest.com/")


class LiveBet(unittest.TestCase):
    """ RG真人下注 """

    l_baccarat_ab = Baccarat.LiveBaccaratAutoBet(driver, get_img)
    l_dragontiger_ab = DragonTiger.LiveDragonTigerAutoBet(driver, get_img)

    def test_login(self):
        """ 登入 """

        result = True

        try:
            get_img("首頁")
            # 點擊語系
            WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//span[@class='txt_lang']"))).click()
            WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//span[text()='繁體中文']"))).click()
            input_acc = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//input[@class='ipt_login' and @placeholder='請輸入帳號']")))
            input_acc.send_keys(account[0])

            input_pwd = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//input[@class='ipt_login' and @placeholder='請輸入密碼']")))
            input_pwd.send_keys(account[1])

            WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//button[@type='submit']"))).click()
            time.sleep(1)

            # 判斷輸入框隱藏
            WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//form[@class='are_head' and @style='display: none;']")))

            check_name = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//div[@class='ui_username']")))

            if account[0] not in check_name.text:
                print(f"Fail: 登入失敗，登入帳號:{account[0]}，顯示帳號為:{check_name.text}")
                result = False

            get_img("登入成功")
        except:
            get_img("登入失敗")
            result = False

        self.assertEqual(True, result)

    def test_live_baccarat(self):
        """ 百家樂自動下注 """

        result = self.l_baccarat_ab.auto_bet()
        self.assertEqual(True, result)

    def test_live_dragontiger(self):
        """ RG真人_龍虎自動下注 """

        result = self.l_dragontiger_ab.auto_bet()
        self.assertEqual(True, result)


if __name__ == '__main__':
    test_units = unittest.TestSuite()
    test_units.addTests([
        LiveBet("test_login"),
        LiveBet("test_live_baccarat"),
        LiveBet("test_live_dragontiger"),
    ])

    now = datetime.now().strftime('%m-%d %H_%M_%S')
    root_path = os.getcwd()
    filename = now + '.html'
    with open(filename, 'wb+') as fp:
        runner = HTMLTestRunner(
            stream=fp,
            verbosity=2,
            title='自動下注',
            driver=driver
        )
        runner.run(test_units)

    driver.quit()
