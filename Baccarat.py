# _*_ coding: UTF-8 _*_
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time

# [ "23", "2K", "19", "38" ] 莊家、小
# [ "33", "27", "38", "17","37","1A" ] 閒家、大、莊對、任意對子
# [ "24", "3A", "23", "17" ] 和局、小
# [ "27", "11", "27", "16","24" ] 莊家、閒對、任意對子、完美對子、大
# [ "22", "24", "41", "22", "12" ] (免傭)莊家、大
# [ "22", "31", "14", "45" ] (免傭)和局、小
# [ "13", "23", "13", "23" ] 和局、閒對、莊對、小、任意對子、完美對子

game_round1 = ["banker", "small"]                             # 莊家、小
game_round2 = ["big", "any-pair", "banker-pair", "player"]   # 任意對子、大、莊對、閒家
game_round3 = ["tie"]                                         # 和局
game_round4 = ["player-pair", "perfect-pair"]                # 閒對、完美對子

all_game_round = {
    "game_round1": False,
    "game_round2": False,
    "game_round3": False,
    "game_round4": False
}

class LiveBaccaratAutoBet:
    """ RG真人_百家樂自動下注 """

    def __init__(self, driver, get_img):
        self.driver = driver
        self.get_img = get_img

    def check_all_round(self, all_game):
        """
        確認所有下注注區是否為 True
        參數:
            game (dict): 遊戲注區

        Returns:
            bool: 判斷全部成功回傳 True，失敗則是 False
        """

        result_list = [all_game[i] for i in all_game]
        return False if "False" in str(result_list) else True

    def check_bet_success(self):
        """ 確認下注是否成功 """

        try:
            # 點擊確認
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//img[@src='img/confirm_button.5fe259e2.svg']"))).click()

            # 判斷下注成功提示是否有出現
            WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//div[contains(text(), '下注成功')]")))
            self.get_img("下注成功")

        except:
            print("下注失敗\n")

    def auto_bet(self):
        """ 自動下注 """

        bet_finish = False
        result_list = ["True"]

        # 點擊真人類別->進入RG真人
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//span[text()='真人']"))).click()
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//div[@class='reg_live rg']//a[@class='lnk_enterGame']"))).click()

        # wait loading
        WebDriverWait(self.driver, 10).until(EC.invisibility_of_element_located(
            (By.XPATH, "//div[@class='box_bg box_loading' and not(contains(@style, 'none'))]")))
        time.sleep(1)

        # 切換分頁
        handles = self.driver.window_handles
        self.driver.switch_to.window(handles[-1])

        # 點擊進入百家樂，桌號P-A
        try:
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[@class='desk-info']/div[./span[text()=' 百家樂 P-A']]/.."))).click()
            time.sleep(2)

        except Exception as e:
            self.get_img("Fail：進入百家樂C-A失敗")
            print(e)
            return False

        # 關閉現場音效
        try:
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[@class='button no']"))).click()

        except:
            pass

        # 使用迴圈來下注，當所需下注牌型四局下完才結束迴圈
        while not bet_finish:

            bet_finish = self.check_all_round(all_game_round)

            # 抓取當前局號
            now_round = WebDriverWait(self.driver, 30).until(EC.presence_of_element_located(
                (By.XPATH, "//div[@class='game-name']"))).text
            bet_round = (int(now_round[-4:]) % 7)

            # 撈取倒數秒數
            bet_sec = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//div[@class='timer-wrapper']"))).text

            # 當倒數秒數不為空且大於3秒時才下注
            if not bet_sec == "" and int(bet_sec) > 3:

                # 若當局餘數=1
                if bet_round == 1:
                    print("下注注區：莊家、小")
                    for i in game_round1:
                        WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                            (By.XPATH, f"//*[name()='svg' and @class='touch-panel']//*[name()='path' and @class='{i}']"))).click()
                    self.check_bet_success()
                    all_game_round["game_round1"] = True

                # 若當局餘數=2
                elif bet_round == 2:
                    print("下注注區：閒家、大、莊對、任意對子")
                    for i in game_round2:
                        WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                            (By.XPATH,f"//*[name()='svg' and @class='touch-panel']//*[name()='path' and @class='{i}']"))).click()
                    self.check_bet_success()
                    all_game_round["game_round2"] = True

                # 若當局餘數=3
                elif bet_round == 3:
                    print("下注注區：和局")
                    for i in game_round3:
                        WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                            (By.XPATH,f"//*[name()='svg' and @class='touch-panel']//*[name()='path' and @class='{i}']"))).click()
                    self.check_bet_success()
                    all_game_round["game_round3"] = True

                # 若當局餘數=4
                elif bet_round == 4:
                    print("下注注區：閒對、完美對子")
                    for i in game_round4:
                        WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                            (By.XPATH,f"//*[name()='svg' and @class='touch-panel']//*[name()='path' and @class='{i}']"))).click()
                    self.check_bet_success()
                    all_game_round["game_round4"] = True

                # 等待新局出現
                WebDriverWait(self.driver, 30).until(EC.presence_of_element_located(
                    (By.XPATH, "//div[contains(text(), '已開局，請下注')]")))
                time.sleep(2)

            else:
                # 等待新局出現
                WebDriverWait(self.driver, 30).until(EC.presence_of_element_located(
                    (By.XPATH, "//div[contains(text(), '已開局，請下注')]")))
                time.sleep(2)

        results = False if "False" in str(result_list) else True
        return results